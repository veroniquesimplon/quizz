// questions est un objet avec 2 propriétes : question et answers
//Anwser est aussi un objet avec 4 propriétés : text 
const questions = [
    {
        question: "Comment dire en anglais le mot réserver ?",
        answers: [
            { text: "To plan", correct: false },
            { text: "To book", correct: true },
            { text: "To order", correct: false },
            { text: "To purchase", correct: false },
        ]
    },
    {
        question: "Lequel de ces mots désigne le quai d'une gare en anglais ?",
        answers: [
            { text: "Station", correct: false },
            { text: "Coach", correct: false },
            { text: "Railway", correct: false },
            { text: "Platform", correct: true },
        ]
    },
    {
        question: "Comment dire en anglais :\"Il y a un ascenseur juste derrière vous.\"",
        answers: [
            { text: "There's a lift just in front of you.", correct: false },
            { text: "There's a lift just next to you.", correct: false },
            { text: "There's a lift just behind you.", correct: true },
            { text: "There's a lift on the ground floor.", correct: false },
        ]
    },
    {
        question: "Comment traduire le mot mûre en anglais ?",
        answers: [
            { text: "Blueberry", correct: false },
            { text: "Cherry", correct: false },
            { text: "Raspberry", correct: false },
            { text: "Blackberry", correct: true },
        ]
    },
    {
        question: "Forgive, Forgave, ... ",
        answers: [
            { text: "Forgotten", correct: false },
            { text: "Forbidden", correct: false },
            { text: "Forgiving", correct: false },
            { text: "Forgiven", correct: true },
        ]
    },
    {
        question: "Comment dire en anglais “j'ai soif” ?",
        answers: [
            { text: "I'm hungry", correct: false },
            { text: "I'm tired", correct: false },
            { text: "I'm thirsty", correct: true },
            { text: "I'm pissed", correct: false },
        ]
    },
    {
        question: "... game do you want to play ?",
        answers: [
            { text: "What", correct: false },
            { text: "Whose", correct: false },
            { text: "How", correct: false },
            { text: "Which", correct: true },
        ]
    },
    {
        question: "Comment dire \"prune\" en anglais ?",
        answers: [
            { text: "Peach", correct: false },
            { text: "Plum", correct: true },
            { text: "Pickle", correct: false },
            { text: "Pumpkin", correct: false },
        ]
    },
    {
        question: "Comment écrire \"mercredi\" en anglais ?",
        answers: [
            { text: "Wendesday", correct: false },
            { text: "Wednesday", correct: true },
            { text: "Wenesday", correct: false },
            { text: "Wesnasday", correct: false },
        ]
    },
    {
        question: "Comment écrire en anglais:\"Il est huit heures vingt.\" ?",
        answers: [
            { text: "It's twenty to eight.", correct: false },
            { text: "It's eight and twenty passed", correct: false },
            { text: "It's twenty past eight.", correct: true },
            { text: "It's eight hours twenty.", correct: false },
        ]
    },
];



const question = document.querySelector("#question");
const buttonNext = document.getElementById("bouttonNext");
const uneReponse = document.getElementsByClassName("uneReponse");
const scoreFinal = document.querySelector<HTMLElement>("#score");
const quizz = document.querySelector<HTMLElement>(".quizz");
const blockScore = document.querySelector<HTMLElement>(".blockScore"); 



let numQ = 0; 
let score = 0;

/*
question.innerHTML = questions[0].question;
uneReponse[0].innerHTML = questions[0].answers[0].text;
uneReponse[1].innerHTML = questions[0].answers[1].text;
uneReponse[2].innerHTML = questions[0].answers[2].text;
uneReponse[3].innerHTML = questions[0].answers[3].text; même chose que le for qui suit*/

let isNotRep = true;


Next()

function Next() {
    question.innerHTML = questions[numQ].question;//question de HTML sera remplacé par question de questions
    for (let i = 0; i < uneReponse.length; i++) { //lance la prochaine questions jusqu'à ce qui n'en est plus
        uneReponse[i].setAttribute("style", "background-color: white;")// bg color des réponses en blanc
        uneReponse[i].innerHTML = questions[numQ].answers[i].text; //uneReponse de HTML sera remplacé par text de answers de questions
        uneReponse[i].addEventListener("click", () => { // click sur un réponse juste -> score 1+ et le bg color devient vert 
            if (isNotRep) {
                if (questions[numQ].answers[i].correct) {
                    uneReponse[i].setAttribute("style", "background-color: #A2FFAA;")
                    score++;
                    //console.log(score);
                    isNotRep = false;
                } else { // click sur fausse réponse -> bg color red/rose
                    isNotRep = false;
                    //console.log("false");
                    uneReponse[i].setAttribute("style", "background-color: #FFB3A2;");
                    for (let i = 0; i < uneReponse.length; i++) { // click sur la bonne réponse-> bg color vert
                        if (questions[numQ].answers[i].correct) {
                            uneReponse[i].setAttribute("style", "background-color: #A2FFAA;")
                        }
                    }
                }
                buttonNext.style.display = "block"; // button apparaît après avoir sélectionné une réponse
            }
        })
    }
}

buttonNext.addEventListener("click", () => { // click sur button next -> affiachage de la prochaine question et réponses
    if (questions[numQ + 1]) {
        numQ++
        Next();
        buttonNext.style.display = "none";
        isNotRep = true;
    } else {// si plus de questions à la suite -> cache le quizz et affichage du blockScore avec le score
        quizz.style.display = "none";
        blockScore.style.display = "block";
        blockScore.textContent = "Voici ton score : "+score+"/10"; // ce qui est affiché dans le blockScore
        blockScore.style.display = "flex";
    }
})

/*
1. Comment dire en anglais le mot réserver ? OK 
a. to plan 
b. to book (right)
c. to order 
d. to purchase

2. Lequel de ces mots désigne le quai d'une gare en anglais ? OK
a. station (la gare)
b. coach (le wagon)
c. railway (la voie de chemin de fer)
d. platform (right)

3. Comment dire en anglais :"Il y a un ascenseur juste derrière vous." OK
a. There's a lift just in front of you.
b. There's a lift just next to you.
c. There's a lift just behind you. (right)
d. There's a lift on the ground floor.

4. Comment traduire le mot mûre en anglais ? OK
a. Blueberry (myrtille)
b. Cherry
c. Raspberry (framboise)
d. Blackberry (mûre) (right)


5. Forgive, Forgave, ... OK 
a. Forgotten
b. Forbidden
c. Forgiving
d. Forgiven (right)

6. Comment dire en anglais “j'ai soif” ? OK
a. I'm hungry
b. I'm tired
c. I'm thirsty (right)
d. I'm pissed

7. "... game do you want to play ?" OK
a. What
b. Whose
c. How
d. Which (right)

8. Commen dire "prune" en anglais ? OK
a. Peach
b. Plum (right)
c. Pickle
d. Pumpkin

9. Comment écrire "mercredi" en anglais ?
a. Wendesday
b. Wednesday (right)
c. Wenesday
d. Wesnasday


10. Comment écrire en anglais:"Il est huit heures vingt." ?
a. It's twenty to eight.
b. It's eight and twenty passed.
c. It's twenty past eight.
d. It's eight hours twenty.

*/